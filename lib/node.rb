# frozen_string_literal: true

class Node
  attr_accessor :value, :parent_node, :left_child_node, :right_child_node

  def initialize
    @value = nil
    @parent_node = nil
    @left_child_node = nil
    @right_child_node = nil
  end
end
