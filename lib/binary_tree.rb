# frozen_string_literal: true

class BinaryTree
  require_relative 'node.rb'

  attr_reader :root

  def initialize
    @root = nil
  end

  public

  def build_tree(values)
    values.each do |value|
      insert(value)
    end
  end

  def inorder(root)
    inorder(root.left_child_node) unless root.left_child_node.nil?
    puts root.value
    inorder(root.right_child_node) unless root.right_child_node.nil?
  end

  def insert(value)
    @root = add_node(@root, value)
  end

  private

  def add_node(root, value)
    node = Node.new
    node.value = value

    if root.nil?
      root = node
    else
      if root.value < node.value
        root.right_child_node = add_node(root.right_child_node, value)
        root.right_child_node.parent_node = root
      else
        root.left_child_node = add_node(root.left_child_node, value)
        root.left_child_node.parent_node = root
      end
    end
    root
  end
end
