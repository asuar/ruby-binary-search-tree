# frozen_string_literal: true

class Crawler
  require_relative 'binary_tree.rb'

  def initialize(tree)
    @binary_tree = tree
  end

  public

  def breadth_first_search(value)
    return @binary_tree.root if @binary_tree.root.value == value

    node_list = []
    current_node = @binary_tree.root

    until current_node.value == value
      node_list << current_node.left_child_node unless current_node.left_child_node.nil?
      node_list << current_node.right_child_node unless current_node.right_child_node.nil?
      current_node = node_list.shift
      return nil if current_node.nil?
    end
    current_node
  end

  def depth_first_search(value)
    return @binary_tree.root if @binary_tree.root.value == value

    node_list = []
    current_node = @binary_tree.root

    until current_node.value == value
      node_list.push current_node.left_child_node unless current_node.left_child_node.nil?
      node_list.push current_node.right_child_node unless current_node.right_child_node.nil?
      current_node = node_list.pop
      return nil if current_node.nil?
    end
    current_node
  end

  def dfs_rec(root, value)
    return root if root.value == value

    left = dfs_rec(root.left_child_node, value) unless root.left_child_node.nil?
    right = dfs_rec(root.right_child_node, value) unless root.right_child_node.nil?
    left.nil? ? right : left
  end
end

tree = BinaryTree.new
tree.build_tree([1, 7, 4, 23, 8, 9, 4, 3, 5, 7, 9, 67, 6345, 324])
crawler = Crawler.new(tree)
node = crawler.breadth_first_search(89)
node = crawler.depth_first_search(7)
node = crawler.dfs_rec(tree.root, 9)
puts node.parent_node.value
# puts tree.inorder(tree.root)
