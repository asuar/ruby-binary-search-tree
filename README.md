# ruby-binary-search-tree

Created with Ruby as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/ruby-programming/lessons/data-structures-and-algorithms). 

# Final Thoughts

Completing this project allowed me to practice with Binary Search Trees, Breadth First Search, Depth First Search and Object Oriented Programming in Ruby.
